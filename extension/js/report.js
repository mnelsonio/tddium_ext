jQuery(function(){

  var TddiumExt = function(options){
    this.options = options || {};
    this.bind_events();
    this.current_tentry_id = null;
    this.locator = $('<span id="tddium_ext_locator"></span>');
    $(document.body).append(this.locator);

    this.tentry_class = 'tddium_ext_current_tentry';
  };

  TddiumExt.prototype = {
    bind_events : function(){
      $(document.body).keydown($.proxy(this.keydown, this));
      $(document.body).keyup($.proxy(this.keyup, this));
    },
    search : function(){
      return $('.dataTables_filter input[type="text"]:visible');
    },
    go_to_search : function(){
      if(this.search().is(':focus'))
        return false;

      this.search().focus();
      this.adjust_scrolltop(this.search());
      return true;
    },
    current_tentry : function(skip_setting){
      var current = $('.tentry.' + this.tentry_class + ':visible');
      if(current.length > 0)
        return current;
      else if(!skip_setting)
        return this.set_current_tentry($('.tentry:first-child:visible'));
    },
    set_current_tentry : function(entry, scroll){
      if(scroll === undefined) scroll = true;
      $('.' + this.tentry_class).removeClass(this.tentry_class);
      entry.addClass(this.tentry_class);
      this.adjust_scrolltop();
      this.current_tentry_id = null;
      return entry;
    },
    adjust_scrolltop : function(entry){
      entry = entry || this.current_tentry(true);
      if(entry.length == 1){
        var viewport  = $(window);

        var top       = viewport.scrollTop();
        var bottom    = this.locator.offset().top;
        var view_height = bottom - top;
        var padding   = view_height * 0.1;

        var entry_top = entry.offset().top;
        var entry_bot = entry_top + entry.height();

        if(entry_top > bottom - padding)
          viewport.scrollTop(entry_bot - (view_height - padding));
        else if(entry_bot < top + padding)
          viewport.scrollTop(entry_top - padding);
        
      }
    },
    activate_tentry : function(){
      var current_id = this.current_tentry_id = this.current_tentry().attr('id');
      this.current_tentry().click();

      var func = $.proxy(function(){
        if(this.current_tentry_id){
          var should_be = $('#' + this.current_tentry_id + ':not(.' + this.tentry_class + ')');
          if(should_be.length > 0){
            this.set_current_tentry(should_be, false);
          }
        }
      }, this);

      // ensures long requests are taken care of.
      setTimeout(func, 20);
      setTimeout(func, 80);
      setTimeout(func, 300);
      setTimeout(func, 900);
      setTimeout(func, 1200);
      
    },
    handle_escape : function(){
      if(this.search().is(':focus'))
        this.search().blur();
      else
        $('.tdetail').prev('.tentry').click();
    },
    copy_spec_path : function() {

      if(this.search().is(':focus'))
        return;

      var current_entry = this.current_tentry();
      var full_path = $(current_entry.children('td')[1]).children('div').first().html();
      var details = current_entry.next('.tdetail').find('div.scrollpane');
      var pattern = /rspec \.\/spec.*:([0-9]{1,5})/i;
      var line_number = "";

      if(details !== undefined) {
        var results = pattern.exec(details.html());

        console.log(results);

        if(results && results.length == 2) line_number = ":" + results[1];
      }

      full_path += line_number;

      this.copy_to_clipboard(full_path);
    },
    copy_to_clipboard : function(text){
      chrome.extension.sendMessage({text_to_copy : text});
    },
    move_up : function(){
      if(this.current_tentry().is(':first-child'))
        return;
      this.set_current_tentry($(this.current_tentry().prevAll('.tentry')[0]));
    },
    move_down : function(){
      if($('.' + this.tentry_class).length === 0)
        return this.move_up();

      if(this.current_tentry().is(':last-child'))
        return;
      this.set_current_tentry($(this.current_tentry().nextAll('.tentry')[0]));
    },
    keydown : function(event){

      var target = $(event.target);

      if(event.which == 27) //esc
        this.handle_escape();
      else if([38, 75].indexOf(event.which) >= 0)   //  up - k
        this.move_up();
      else if([40, 74].indexOf(event.which) >= 0)   //  down - j
        this.move_down();
      else if([32, 13].indexOf(event.which) >= 0)   //  space || enter
        this.activate_tentry();
      else if([84, 191].indexOf(event.which) >= 0)  //  t - /
        if(this.go_to_search()){
          event.preventDefault();
          return false;
        }
      if([38, 40, 32, 13].indexOf(event.which) >= 0){
        event.preventDefault();
        return false;
      }
    },
    keyup : function(event){
      if([67, 88].indexOf(event.which)) //  x
        this.copy_spec_path();
    }
  };


  var ext = new TddiumExt();
});
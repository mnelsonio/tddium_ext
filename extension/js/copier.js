var copyIt = function(text){
  var copyDiv = document.createElement('div');
  copyDiv.contentEditable = true;
  document.body.appendChild(copyDiv);
  copyDiv.innerHTML = text;
  copyDiv.unselectable = "off";
  copyDiv.focus();
  document.execCommand('SelectAll');
  document.execCommand("Copy", false, null);
  document.body.removeChild(copyDiv);
};

chrome.extension.onMessage.addListener(function(request, sender, sendResponse) {
  if(request.text_to_copy)
    copyIt(request.text_to_copy);
});

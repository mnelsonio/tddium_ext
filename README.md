## Tddium Chrome Extension

Removes the extra cruft. Provide reports with keyboard navigation. Changes the dashboard and the report views from:

![Dashboard Before](https://raw.github.com/mnelson/tddium_ext/master/screenshots/dash_before.png "Dashboard Before")

![Report Before](https://raw.github.com/mnelson/tddium_ext/master/screenshots/report_before.png "Report Before")

---

to:

---

![Dashboard After](https://raw.github.com/mnelson/tddium_ext/master/screenshots/dash_after.png "Dashboard After")

![Report After](https://raw.github.com/mnelson/tddium_ext/master/screenshots/report_after.png "Report After")


### Keyboard Navigation

While in a report view you can use `up or k`, `down or j`, `enter`, `space`, and `escape` to navigate specs. Hitting `t or /` will bring you to the search field.

While in the search box you can use `esc` to exit it.

While viewing a test result (either opened or closed) use `x` to copy the test path to clipboard.

### Installation

Clone this repo to your machine. Open up `chrome://chrome/extensions` and install the `extension` directory as an unpacked extension.